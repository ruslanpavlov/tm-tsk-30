package ru.tsc.pavlov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.enumerated.Sort;
import ru.tsc.pavlov.tm.enumerated.UserRole;
import ru.tsc.pavlov.tm.model.Project;
import ru.tsc.pavlov.tm.util.StringUtil;
import ru.tsc.pavlov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class ProjectListCommand extends AbstractProjectCommand {

    @Override
    public UserRole[] roles() {
        return UserRole.values();
    }

    @NotNull
    @Override
    public String getName() {
        return TerminalConst.PROJECT_LIST;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Display list of projects";
    }

    @Override
    public void execute() {
        System.out.println("Enter sort");
        System.out.println(Arrays.toString(Sort.values()));

        @Nullable final String sort = TerminalUtil.nextLine();
        @NotNull final List<Project> projects;
        @Nullable final String userId = getAuthService().getCurrentUserId();
        if (!StringUtil.isEmpty(sort)) {
            Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            projects = getProjectService().findAll(userId, sortType.getComparator());
        }
        else {
            projects = getProjectService().findAll(userId);
        }
        for (Project project : projects) showProject(project);
    }

}
