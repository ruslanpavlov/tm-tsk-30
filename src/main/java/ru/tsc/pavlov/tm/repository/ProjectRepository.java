package ru.tsc.pavlov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.api.repository.IProjectRepository;
import ru.tsc.pavlov.tm.enumerated.Status;
import ru.tsc.pavlov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.pavlov.tm.model.Project;

public final class ProjectRepository extends AbstractOwnerRepository<Project> implements IProjectRepository {

    @Override
    public boolean existsByIndex(@NotNull final String userId, @NotNull final int index) {
        return index < list.size() && index >= 0;
    }

    @NotNull
    @Override
    public Project findByName(@NotNull final String userId, @NotNull final String name) {
        return findAll(userId).stream()
                .filter(p -> p.getName().equals(name))
                .findFirst()
                .orElseThrow(ProjectNotFoundException::new);
    }

    @Override
    public Project findByIndex(@NotNull final String userId, @NotNull final int index) {
        return list.get(index);
    }

    @Nullable
    @Override
    public Project startById(@NotNull final String userId, @NotNull final String id) {
        final Project project = findById(id);
        if (project == null)
            return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @NotNull
    @Override
    public Project startByIndex(@NotNull final String userId, @NotNull final Integer index) {
        final Project project = findByIndex(userId, index);
        if (project == null)
            return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @NotNull
    @Override
    public Project startByName(@NotNull final String userId, @NotNull final String name) {
        final Project project = findByName(userId, name);
        if (project == null)
            return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Nullable
    @Override
    public Project finishById(@NotNull final String userId, @NotNull final String id) {
        final Project project = findById(id);
        if (project == null)
            return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @NotNull
    @Override
    public Project finishByIndex(@NotNull final String userId, @NotNull final Integer index) {
        final Project project = findByIndex(userId, index);
        if (project == null)
            return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @NotNull
    @Override
    public Project finishByName(@NotNull final String userId, @NotNull final String name) {
        final Project project = findByName(userId, name);
        if (project == null)
            return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Nullable
    @Override
    public Project changeStatusById(@NotNull final String userId, @NotNull final String id, @NotNull final Status status) {
        final Project project = findById(id);
        if (project == null)
            return null;
        project.setStatus(status);
        return project;
    }

    @NotNull
    @Override
    public Project changeStatusByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final Status status) {
        final Project project = findByIndex(userId, index);
        if (project == null)
            return null;
        project.setStatus(status);
        return project;
    }

    @NotNull
    @Override
    public Project changeStatusByName(@NotNull final String userId, @NotNull final String name, @NotNull final Status status) {
        final Project project = findByName(userId, name);
        if (project == null)
            return null;
        project.setStatus(status);
        return project;
    }

}
