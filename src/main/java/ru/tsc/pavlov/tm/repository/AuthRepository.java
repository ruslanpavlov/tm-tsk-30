package ru.tsc.pavlov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.pavlov.tm.api.repository.IAuthRepository;

public class AuthRepository implements IAuthRepository {

    @NotNull
    private String currentUserId;

    @Override
    public String getCurrentUserId() {
        return currentUserId;
    }

    @Override
    public void setCurrentUserId(@NotNull final String currentUserId) {
        this.currentUserId = currentUserId;
    }

}
