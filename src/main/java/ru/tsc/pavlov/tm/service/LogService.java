package ru.tsc.pavlov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.api.service.ILogService;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.*;

public class LogService implements ILogService {

    @NotNull private static final String FILE_NAME = "/logger.properties";

    @NotNull private static final String COMMANDS = "COMMANDS";
    @NotNull private static final String COMMANDS_FILE = "./commands.log";

    @NotNull private static final String ERRORS = "ERRORS";
    @NotNull private static final String ERRORS_FILE = "./errors.log";

    @NotNull private static final String MESSAGES = "MESSAGES";
    @NotNull private static final String MESSAGES_FILE = "./messages.log";


    @NotNull private final LogManager manager = LogManager.getLogManager();
    @NotNull private final ConsoleHandler consoleHandler = getConsoleHandler();
    @NotNull private final Logger root = Logger.getLogger("");
    @NotNull private final Logger commands = Logger.getLogger(COMMANDS);
    @NotNull private final Logger messages = Logger.getLogger(MESSAGES);
    @NotNull private final Logger errors = Logger.getLogger(ERRORS);

    {
        init();
        registry(errors, ERRORS_FILE, true);
        registry(commands, COMMANDS_FILE, false);
        registry(errors, ERRORS_FILE, true);
    }

    private ConsoleHandler getConsoleHandler() {
        final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    private void init() {
        try {
            @NotNull final InputStream inputStream = LogService.class.getResourceAsStream(FILE_NAME);
            manager.readConfiguration(inputStream);
        } catch (IOException e) {
            root.severe(e.getMessage());
        }
    }

    private void registry(@NotNull final Logger logger, @NotNull final String filename, final boolean isConsole) {
        try {
            if (isConsole) logger.addHandler(consoleHandler);
            logger.setUseParentHandlers(false);
            logger.addHandler(new FileHandler(filename));
        } catch (@NotNull final IOException e) {
            root.severe(e.getMessage());
        }
    }

    @Override
    public void info(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        messages.info(message);
    }

    @Override
    public void debug(@Nullable String message) {
        if (message == null || message.isEmpty()) return;
        messages.info(message);
    }

    @Override
    public void command(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        messages.fine(message);
    }

    @Override
    public void error(@Nullable Exception e) {
        if (e == null) return;
        errors.log(Level.SEVERE, e.getMessage(), e);
    }

}
