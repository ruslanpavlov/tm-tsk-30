package ru.tsc.pavlov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.api.repository.ITaskRepository;
import ru.tsc.pavlov.tm.api.service.ITaskService;
import ru.tsc.pavlov.tm.enumerated.Status;
import ru.tsc.pavlov.tm.exception.empty.EmptyDescriptionException;
import ru.tsc.pavlov.tm.exception.empty.EmptyIdException;
import ru.tsc.pavlov.tm.exception.empty.EmptyIndexException;
import ru.tsc.pavlov.tm.exception.empty.EmptyNameException;
import ru.tsc.pavlov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.pavlov.tm.exception.system.IndexIncorrectException;
import ru.tsc.pavlov.tm.model.Task;


import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class TaskService extends AbstractOwnerService<Task> implements ITaskService {

    @NotNull private final ITaskRepository taskRepository;

    public TaskService(@NotNull final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(@Nullable final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Task task = new Task();
        task.setName(name);
        taskRepository.add(task);
    }

    @Override
    public void create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final Task task = new Task();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
    }

    @NotNull
    @Override
    public Task findByName(@Nullable final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new TaskNotFoundException();
        return taskRepository.findByName(userId, name);
    }

    @NotNull
    @Override
    public Task findById(@Nullable String userId, @Nullable String id) {
        return taskRepository.findById(userId, id);
    }

    @NotNull
    @Override
    public Task findByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) return null;
        if (index < 0 || index > taskRepository.getSize()) throw new IndexIncorrectException();
        return taskRepository.findByIndex(userId, index);
    }

    @NotNull
    @Override
    public void remove(@Nullable String userId, @Nullable Task entity) {

    }

    @NotNull
    @Override
    public Integer getSize(@Nullable String userId) {
        return null;
    }

    @NotNull
    @Override
    public Task removeByName(@Nullable final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyIdException();
        return taskRepository.removeByName(userId, name);
    }

    @NotNull
    @Override
    public Task removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (index < 0 || index > taskRepository.getSize()) throw new IndexIncorrectException();
        return taskRepository.removeByIndex(userId, index);
    }

    @Override
    public void updateById(
            @Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Task task = taskRepository.findById(id);
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
    }

    @Override
    public void updateByIndex(
            @Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @Nullable final String description
    ) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (index < 0 || index > taskRepository.getSize()) throw new IndexIncorrectException();
        @NotNull final Task task = taskRepository.findByIndex(userId, index);
        task.setName(name);
        task.setDescription(description);
    }

    @NotNull
    @Override
    public boolean existsByIndex(@Nullable final String userId, @Nullable final int index) {
        return taskRepository.existsByIndex(userId, index);
    }

    @NotNull
    @Override
    public boolean existsByName(@Nullable final String userId, @Nullable final String name) {
        return taskRepository.existsByName(userId, name);
    }

    @NotNull
    @Override
    public Task startById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.startById(userId, id);
    }

    @NotNull
    @Override
    public Task startByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (index < 0 || index > taskRepository.getSize()) throw new IndexIncorrectException();
        return taskRepository.startByIndex(userId, index);
    }

    @NotNull
    @Override
    public Task startByName(@Nullable final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.startByName(userId, name);
    }

    @NotNull
    @Override
    public Task finishById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.finishById(userId, id);
    }

    @NotNull
    @Override
    public Task finishByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (index < 0 || index > taskRepository.getSize()) throw new IndexIncorrectException();
        return taskRepository.finishByIndex(userId, index);
    }

    @NotNull
    @Override
    public Task finishByName(@Nullable final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.finishByName(userId, name);
    }

    @NotNull
    @Override
    public Task changeStatusById(@Nullable final String userId, @Nullable final String id, @Nullable Status status) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) status = Status.valueOf("IN PROGRESS");
        return taskRepository.changeStatusById(userId, id, status);
    }

    @NotNull
    @Override
    public Task changeStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable Status status) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (index < 0 || index > taskRepository.getSize()) throw new IndexIncorrectException();
        if (status == null) status = Status.valueOf("IN PROGRESS");
        return taskRepository.changeStatusByIndex(userId, index, status);
    }

    @NotNull
    @Override
    public Task changeStatusByName(@Nullable final String userId, @Nullable final String name, @Nullable Status status) {
        if (name == null || name.isEmpty()) return null;
        if (status == null) status = Status.valueOf("IN PROGRESS");
        return taskRepository.changeStatusByName(userId, name, status);
    }

}
