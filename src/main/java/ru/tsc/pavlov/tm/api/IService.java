package ru.tsc.pavlov.tm.api;

import ru.tsc.pavlov.tm.model.AbstractEntity;

public interface IService<E extends AbstractEntity> extends IRepository<E>  {

}
