package ru.tsc.pavlov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.enumerated.UserRole;
import ru.tsc.pavlov.tm.model.User;


public interface IAuthService {

    @NotNull
    String getCurrentUserId();

    void setCurrentUserId(@Nullable String userId);

    void setUser(String currentUserId);

    boolean isUserAuth();

    boolean isUserAdmin();

    User login(@Nullable String login, @Nullable String password);

    void logout();

    void checkRoles(@Nullable UserRole... roles);

}
