package ru.tsc.pavlov.tm.exception.entity;

import ru.tsc.pavlov.tm.exception.AbstractException;

public class TaskNotFoundException extends AbstractException {

    public TaskNotFoundException() {
        super("Error. Task not found.");
    }

    public TaskNotFoundException(String value) {
        super("Error. Task " + value + " not found.");
    }

}
