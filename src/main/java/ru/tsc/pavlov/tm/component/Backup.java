package ru.tsc.pavlov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


public class Backup {

    private static final String COMMAND_SAVE = "backup-save";

    private static final String COMMAND_LOAD = "backup-load";

    private static final int INTERVAL = 30;

    private final Bootstrap bootstrap;

    @NotNull
    private final ScheduledExecutorService executorService;


    public Backup(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        executorService = Executors.newSingleThreadScheduledExecutor();
    }

    public void init() {
        load();
        executorService.scheduleWithFixedDelay(this::save, 0, INTERVAL, TimeUnit.SECONDS);
    }

    public void stop() {
        executorService.shutdown();
    }

    @SneakyThrows
    public void save() {
        bootstrap.parseCommand(COMMAND_SAVE);
    }

    @SneakyThrows
    public void load() {
        bootstrap.parseCommand(COMMAND_LOAD);
    }

}
