package ru.tsc.pavlov.tm;

import org.jetbrains.annotations.NotNull;
import ru.tsc.pavlov.tm.component.Bootstrap;
import ru.tsc.pavlov.tm.util.SystemUtil;

public class Application {

    public static void main(final String[] args) {
        System.out.println("PID: " + SystemUtil.getPID());
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
    }

}
